// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console
function ArrangeAlphabetically(carModels) {
    let arrayLength = carModels.length;

    for (let index = 0; index < arrayLength - 1; index++) {
        for (let index2 = index + 1; index2 < arrayLength; index2++) {
            if (carModels[index] > carModels[index2]) {
                let temporary_var = carModels[index];
                carModels[index] = carModels[index2];
                carModels[index2] = temporary_var;
            }
        }
    }
    return carModels;
}

function sortCarModelsAlphabetically(inventory) {
    if (Array.isArray(inventory)) {
        let carModels = [];
        for (let objects of inventory) {
            carModels.push(objects["car_model"]);
        }
        let sortedCarModels = ArrangeAlphabetically(carModels);
        return sortedCarModels;
    }
    else {
        return null;
    }
}
module.exports = { sortCarModelsAlphabetically };
