// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console
function carYear(inventory) {
    if (Array.isArray(inventory)) {
        let carYearList = [];
        for (const car of inventory) {
            carYearList.push(car["car_year"]);
        }
        return carYearList;

    }
    else {
        return null;
    }
}

module.exports = { carYear };