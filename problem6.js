// A buyer is interested in seeing only BMW and Audi cars within the inventory. Execute a function and return an array that only contains BMW and Audi cars. 
function bmwAndAudiCars(inventory) {
    let carDetails = [];
    if (Array.isArray(inventory)) {
        for (const car of inventory) {
            if (car.car_make == "BMW" || car.car_make == "Audi") {
                carDetails.push(car);
            }
        }
        return carDetails;
    }
    else {
        return null;
    }
}

module.exports = {bmwAndAudiCars};