// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

function carsManufacturedBefore2000(carYearList) {
    if (Array.isArray(carYearList)) {
        let carsListManufacturedBefore2000 = [];
        for (const Manufactureyear of carYearList) {
            if (Manufactureyear < 2000) {
                carsListManufacturedBefore2000.push(Manufactureyear);
            }

        }
        return carsListManufacturedBefore2000;
    }
    else {
        return null;
    }
}

module.exports = { carsManufacturedBefore2000 };