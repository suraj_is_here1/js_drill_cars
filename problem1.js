// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log.
function findCarById(inventory, id) {
    if (Array.isArray(inventory) && typeof (id) == "number") {

        let car_Id_Exist = false;
        for (let index = 0; index < inventory.length; index++) {
            if (inventory[index]["id"] == id) {
                car_Id_Exist = true;
                console.log(`Car ${id} is a ${inventory[index]["car_year"]} ${inventory[index]["car_make"]} ${inventory[index]["car_model"]}`);
            }
        }
        if (car_Id_Exist == false) {
            console.log("Car id not found !");
        }
    }
    else {
        console.log(null);
    }
}
module.exports = { findCarById };

