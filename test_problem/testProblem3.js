const { sortCarModelsAlphabetically } = require("../problem3.js");
const inventory = require("./data.js");
try {
    console.log(`Car model sorted alphabetwise :-` + sortCarModelsAlphabetically(inventory));
}
catch (error) {
    console.error("Something went wrong !");
}


