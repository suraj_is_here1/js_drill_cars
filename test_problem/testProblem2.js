const { findLastCar } = require("../problem2.js");
const inventory = require("./data.js");

try {
    findLastCar(inventory);
}
catch (error) {
    console.error("Something went wrong !");
}