const inventory = require("./data.js");
const { carYear } = require("../problem4.js");
const { carsManufacturedBefore2000 } = require("../problem5.js");

try {
    let carYearList = carYear(inventory);
    console.log(carsManufacturedBefore2000(carYearList).length);
} catch (error) {
    console.log("something went wrong !");
}
