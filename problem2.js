// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is? Log the make and model into the console
function findLastCar(inventory) {
    if (Array.isArray(inventory)) {
        for (let index = 0; index < inventory.length; index++) {
            if (index == inventory.length - 1) {
                console.log(`Last car is a ${inventory[index]["car_make"]} ${inventory[index]["car_model"]}`);
            }
        }
    }
    else {
        console.log(null);
    }

}
module.exports = { findLastCar };
